package ru.otpbank.kafka.dao;

import org.springframework.data.repository.CrudRepository;
import ru.otpbank.kafka.entity.RawDataModel;

public interface RawDataRepository extends CrudRepository<RawDataModel, Long> {

}
