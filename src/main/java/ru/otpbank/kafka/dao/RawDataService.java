package ru.otpbank.kafka.dao;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otpbank.kafka.entity.RawDataModel;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RawDataService {

    private final RawDataRepository rawDataRepository;

    public RawDataModel save(RawDataModel rawMessage) {
        return rawDataRepository.save(rawMessage);
    }

    public RawDataModel updateServiceA(RawDataModel rawMessage) {
        Optional<RawDataModel> update = rawDataRepository.findById(rawMessage.getId());
        update.get().setServiceAResult("Результат работы сервиса А");
        return rawDataRepository.save(update.get());
    }

    public RawDataModel updateServiceB(RawDataModel rawMessage) {
        Optional<RawDataModel> update = rawDataRepository.findById(rawMessage.getId());
        update.get().setServiceBResult("Результат работы сервиса B");
        return rawDataRepository.save(update.get());
    }

    public RawDataModel getById(Long id) {
        return rawDataRepository.findById(id).isPresent() ? rawDataRepository.findById(id).get() : null;
    }

}
