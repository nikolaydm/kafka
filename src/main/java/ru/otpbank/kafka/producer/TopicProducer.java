package ru.otpbank.kafka.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TopicProducer {

    @Value("${topic.name.postman.producer}")
    private String topicNamePostman;

    @Value("${topic.name.serviceA.producer}")
    private String topicNameServiceA;

    @Value("${topic.name.serviceB.producer}")
    private String topicNameServiceB;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendToTopicPostman(String message) {
        log.info("Payload content from Postman: {}", message);
        kafkaTemplate.send(topicNamePostman, message);
    }

    public void sendToTopicServiceA(String message) {
        log.info("Payload content: {}", message);
        kafkaTemplate.send(topicNameServiceA, message);
    }

    public void sendToTopicServiceB(String message) {
        log.info("Payload content: {}", message);
        kafkaTemplate.send(topicNameServiceB, message);
    }

}