package ru.otpbank.kafka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "KAFKA_RAWDATA")
@Getter
@Setter
public class RawDataModel {

    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    public Long id;

    @Column(name = "Data", length = 64, nullable = false)
    public String data;

    @Column(name = "WeekDay", nullable = false)
    public String weekDay;

    @Column(name = "ServiceAResult", nullable = true)
    public String serviceAResult;

    @Column(name = "ServiceBResult", nullable = true)
    public String serviceBResult;

    public RawDataModel() {
    }

    public RawDataModel(String data, String weekDay){
        this.data = data;
        this.weekDay = weekDay;
    }

}
