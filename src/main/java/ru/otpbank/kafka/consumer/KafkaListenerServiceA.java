package ru.otpbank.kafka.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.otpbank.kafka.dao.RawDataService;
import ru.otpbank.kafka.entity.RawDataModel;
import ru.otpbank.kafka.producer.TopicProducer;

@Component
@Slf4j
public class KafkaListenerServiceA {

    @Value("${topic.name.postman.producer}")
    private String topicName;

    private final TopicProducer topicProducer;

    private final RawDataService rawDataService;

    private final ObjectMapper objectMapper;

    public KafkaListenerServiceA(TopicProducer topicProducer, RawDataService rawDataService, ObjectMapper objectMapper) {
        this.topicProducer = topicProducer;
        this.rawDataService = rawDataService;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "${topic.name.postman.producer}", groupId = "myGroup")
    public void processMessage(String content) throws JsonProcessingException, InterruptedException {
        log.info("Message received from topic " + topicName + ": " + content);
        RawDataModel updated = rawDataService.updateServiceA(objectMapper.readValue(content, RawDataModel.class));
        Thread.sleep(5000);
        topicProducer.sendToTopicServiceA(objectMapper.writeValueAsString(updated));
    }

}