package ru.otpbank.kafka.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.otpbank.kafka.dao.RawDataService;
import ru.otpbank.kafka.entity.RawDataModel;
import ru.otpbank.kafka.producer.TopicProducer;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {

    private final RawDataService rawDataService;

    private final TopicProducer topicProducer;

    private final ObjectMapper objectMapper;

    @GetMapping(value = "/send")
    public Long send(@RequestBody RawDataModel payload) throws JsonProcessingException {
        RawDataModel rawDataModel = rawDataService.save(payload);
        topicProducer.sendToTopicPostman(objectMapper.writeValueAsString(rawDataModel));
        return rawDataModel.getId();
    }

    @GetMapping(value = "/get/{id}")
    public RawDataModel getRawData(@PathVariable("id") Long id) {
        return rawDataService.getById(id);
    }

}
